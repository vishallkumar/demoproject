import 'package:flutter/material.dart';
import 'list_title.dart';

class Second extends StatefulWidget {
  const Second({Key? key}) : super(key: key);

  @override
  State<Second> createState() => _SecondState();
}

class _SecondState extends State<Second> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
  appBar:AppBar(
    title: const Text("FLUTTER")
  ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(22),
        child:Column(
          children:[
            Row(
              children: const [
                SizedBox(height: 20,),
                Flexible(
                    child: Icon(Icons.arrow_back_ios,color: Colors.black,
                    )),
                SizedBox(width: 80,),
                Center(
                  child: Flexible(
                      child: Text("Schedule SMS",style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),)),
                ),
                SizedBox(width: 50,),
                Text("Select All",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize:15,
                  color: Colors.blue,
                ),)
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 40),
              child: TextFormField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  hintText: "Search your contact here",
                  prefixIcon: const Icon(Icons.search,color: Colors.black,),
                  hoverColor:Colors.yellow
                ),
                textAlign: TextAlign.left,
              ),
            ),
           ]
        )
      ),
    );
  }
}
